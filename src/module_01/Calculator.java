package module_01;

import java.util.Scanner;

public class Calculator {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.startCalculator();
    }

    private void startCalculator(){
        System.out.print("Введите первое число: ");
        int a = scanner.nextInt();
        System.out.print("Введите второе число: ");
        int b = scanner.nextInt();
        System.out.print("""
                Выберите операцию:\s
                1. Сложение
                2. Вычитание
                3. Умножение
                4. Деление
                """);
        int num;
        num = scanner.nextInt();
        switch (num) {
            case (1) -> sum(a, b);
            case (2) -> difference(a, b);
            case (3) -> multi(a, b);
            case (4) -> division(a, b);
            default -> System.out.println("Не выбрано подходящее действие");
        }
    }

    private static void sum(int a, int b){
        System.out.print("Суммa равна " + (a+b));
    }

    private static void difference(int a, int b){
        System.out.print("Разность равна " + (a-b));
    }

    private static void multi(int a, int b){
        System.out.print("Произведение равно " + (a*b));
    }

    private static void division(int a, int b){
        if (b==0){
            System.out.print("На ноль делить нельзя!");
        }
        else{
            System.out.print("Результат деления " + (a/b));
        }
    }
}