package module_01;

public class Arrays {
    public static void main(String[] args) {
        Arrays arrays = new Arrays();
        arrays.startApplication();
    }

     private void startApplication(){
        int min = -10;
        int max = 10;
        int arraySize = 20;
        int[] arr = new int[arraySize];
        for (int i=0; i<arr.length; i++){
            arr[i] = (int)(Math.random() * (max + 1 - min) + min);
        }
        watchArray(arr);
        changeMaxMin(arr);
        countEvenNumbers(arr);
        setZeroValues(arr);
        increaseElements(arr);
        differentAverageAndMin(arr);
        findRepeated(arr);
    }
    private static void watchArray(int[] a){
        System.out.println("Исходный массив");
        for (int j : a) {
            System.out.print(j + " ");
        }
        System.out.print("\n");
    }

    /*Задание 1. В массиве целых чисел поменять местами максимальный отрицательный элемент
     и минимальный положительный.
     */
    private static void changeMaxMin(int[] a){
        int max = 0;
        int min = 10;
        int index = 0, indexMin=0, indexMax=0;
        //ищем нужные элементы
        for (int j : a) {
            if (j < 0 && max > j) {
                max = j;
                indexMax = index;
            }
            if (j > 0 && min > j) {
                min = j;
                indexMin = index;
            }
            index++;
        }
        a[indexMin] = max;
        a[indexMax] = min;

        //отображаем измененный массив
        for (int j : a) {
            System.out.print(j + " ");
        }
        System.out.println("\nЗамененные элементы: " + min + " " + max);
    }

    //Задание 2. Определить сумму элементов на четных позициях
    private static void countEvenNumbers(int[] a){
        int res = 0;
        for (int i = 1; i<a.length; i+=2){
            res += a[i];
            System.out.print(a[i] + " ");
        }
        System.out.println("\nСумма элементов равна = " + res);
    }

    //Задание 3. Заменить все отрицательные элементы нулями
    private static void setZeroValues(int[] a){
        for (int i=0; i<a.length; i++){
            if (a[i] < 0){
                a[i] = 0;
            }
            System.out.print(a[i] + " ");
        }
        System.out.println("\n");
    }

    //Задание 4. Утроить каждый элемент, который стоит перед отрицательным
    private static void increaseElements(int[] a){
        for(int i=0; i<a.length-1; i++){
            if (a[i+1] < 0){
                a[i] *= 3;
                System.out.print(a[i] + " ");
            }
            else {
                System.out.print(a[i] + " ");
            }
        }
    }

    //Задание 5. Найти разницу между средним арифметическим и значением минимального элемента
    private static void differentAverageAndMin(int[] a){
        int min = a[0];
        int count_elements = 0;
        int average;
        for (int j : a) {
            count_elements += j;
            if (min > j) {
                min = j;
            }
        }
        average = count_elements / a.length;
        System.out.println("Разница между средним арифметическим и значением минимального элемента равна " + (average-min));
    }

    //Задание 6. Вывести все элементы, которые встречаются больше одного раза и индексы которых нечётные.
    private static void findRepeated(int[] a){
        int num;
        int count;
        //считаем одинаковые элементы
        for (int i=0;i<a.length; i++){
            count = 0;
            num = a[i];
            for (int j=i; j<a.length; j++){
                if(a[j] == num){
                    count++;
                }
            }
            //если в текущей итерации элементов больше чем 1, то выводим элемент
            if (count > 1){
                System.out.println("Элемент " + a[i]);
                //ищем нечетные индексы повторяющихся элементов
                for (int k=0; k<a.length; k++){
                    if ((a[k] == a[i]) && ((k + 1) % 2 != 0)){
                        System.out.println("Индекс элемента " + (k+1));
                    }
                }
            }
        }
    }
}
