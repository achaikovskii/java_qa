package module_01;

import java.util.Scanner;

public class Strings {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args){
        Strings strings = new Strings();
        strings.startApplication();
    }

    private void startApplication(){
        System.out.print("Введите количество строк: ");
        int num = scanner.nextInt();
        String[] str = new String[num];
        for (int i=0; i<num; i++){
            System.out.println("Введите строку " + (i+1) + ": ");
            str[i] = scanner.next();
        }

        shortStringLongString(str);
        moreThanAverageString(str, num);
        lessThanAverageString(str, num);
        findWordAnotherSymbols(str);
        onlyAnotherSymbols(str);
        isOnlyDigit(str);
    }

    //Задание 1. Найти самую короткую и самую длинную строки.
    //Вывести найденные строки и их длину.

    private static void shortStringLongString(String[] a){
        int min = a[0].length();
        int max = a[0].length();
        for (String s : a) {
            if (min > s.length()) {
                min = s.length();
            }
            if (max < s.length()) {
                max = s.length();
            }
        }
        System.out.println("Минимальная строка " + min + " символов");
        System.out.println("Максимальная строка " + max + " символов");
    }

    //Задание 2. Вывести на консоль те строки, длина которых больше средней, а также длину.
    private static void moreThanAverageString(String[] a, int n) {
        int sumSymbols = 0;
        int average;
        for (String s : a){
            sumSymbols += s.length();
        }
        average = sumSymbols / n;
        for (String s : a){
            if(s.length() > average){
                System.out.println(s + ". Длина: " + s.length());
            }
        }
    }

    //Задание 3. Вывести на консоль те строки, длина которых меньше средней, а также длину.
    private static void lessThanAverageString(String[] a, int n) {
        int sumSymbols = 0;
        int average;
        for (String s : a){
            sumSymbols += s.length();
        }
        average = sumSymbols / n;
        for (String s : a){
            if(s.length() < average){
                System.out.println(s + ". Длина: " + s.length());
            }
        }
    }

    //Задание 4. Ввести n слов с консоли. Найти слово, в котором число различных символов минимально.
    // Если таких слов несколько, найти первое из них.
    private static void findWordAnotherSymbols(String[] a){
        int min = getDifferentSymbols(a[0]);
        String word = "";
        for (String s : a){
            if(min > getDifferentSymbols(s)){
                min = getDifferentSymbols(s);
                word = s;
            }
        }
        System.out.println("\n\nСлово: " + word + "\nРазличных символов: " + min);
    }

    private static int getDifferentSymbols(String s){
        return (int)s.chars().distinct().count();
    }

    //Задание 5. Найти слово, состоящее только из различных символов.
    // Если таких слов несколько, найти первое из них.
    private static void onlyAnotherSymbols(String[] a){
        for (String s : a){
            if(getDifferentSymbols(s) == s.length()){
                System.out.println(s);
                return;
            }
        }
    }

    //Задание 6. Найти слово, состоящее только из цифр.
    //Если таких слов больше одного, найти второе из них.
    private static void isOnlyDigit(String[] a){
        int count = 0;
        String first = "";
        for (String s : a){
            boolean thisWordNotDigits = true;
            for (int i=0; i<s.length(); i++){
                if(!Character.isDigit(s.charAt(i))){
                    thisWordNotDigits = false;
                    break;
                }
            }
            if (thisWordNotDigits){
                count++;
                if(count == 1){
                    first = s;
                }
                else if(count == 2){
                    System.out.println("Второе слово: " + s);
                }
            }
        }
        if (count == 1){
            System.out.println("Только одно слово: " + first);
        }
        else if (count == 0){
            System.out.println("Слов только из чисел нет");
        }
    }
}
