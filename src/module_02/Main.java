package module_02;
import data.Enum.*;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        startApplication();
    }

    public static void startApplication(){
        Kitchen freeze = new Kitchen("LG", false, 100, 70000, "mom");
        Kitchen gasStove = new Kitchen("Electrolux", true, 150, 30000, "dad");
        Kitchen microwave = new Kitchen("Siemens", false, 70, 8000, "mom");
        Kitchen teapot = new Kitchen("Tefal", false, 90, 3000, "mom");
        Bedroom tv = new Bedroom("Philips", false, 200, 50000);
        Bedroom lamp = new Bedroom("Xiaomi", false, 60, 2000);
        Bedroom charger = new Bedroom("Samsung", true, 250, 500);
        Bedroom phone = new Bedroom("Iphone", true, 10, 45000);
        Bathroom mirror = new Bathroom("Led", false, 50, 3000);
        Bathroom washMachine = new Bathroom("LG", true, 230, 20000);
        Bathroom hydroMassage = new Bathroom("Ariston", true, 380, 5000);
        Bathroom toothBrush = new Bathroom("Colgate", false, 30, 2000);

        ArrayList<Electronics> electronics = new ArrayList<>();
        electronics.add(freeze);
        electronics.add(gasStove);
        electronics.add(microwave);
        electronics.add(teapot);
        electronics.add(tv);
        electronics.add(lamp);
        electronics.add(charger);
        electronics.add(phone);
        electronics.add(mirror);
        electronics.add(washMachine);
        electronics.add(hydroMassage);
        electronics.add(toothBrush);

        findWhereDevices(electronics);
        findAllPower(electronics);
        setOnAllPowerDevices(electronics);
        setOffAllPowerDevices(electronics);
        setOffDevicesOverPower(electronics);
        sortDevicePrices(electronics);
        watchWhoUseDevices(freeze, gasStove, microwave, teapot);
        findDevicesWhichOnAndPower(electronics);
        checkPower(electronics);
        setOnDevicesOverPower(electronics);
        watchAllTechnics(electronics);
        checkStatusOfInternet(electronics);
        findNameOfDevices(electronics);
        calculateDevicesPerPerson(electronics);

        try {
            checkCatchException();
        }
        catch (CustomException e)
        {
            System.out.println("Wow! Поймано какое-то исключение\n" +
                    "Текст ошибки: " + e.getMessage());
        }
    }

    private static void findWhereDevices(ArrayList<Electronics> electronics){
        for (Electronics el : electronics) {
            if (el instanceof Kitchen){
                System.out.println("Kitchen devices - " + el.getName());
            }
            else if (el instanceof Bedroom){
                System.out.println("Bedroom devices - " + el.getName());
            }
            else if (el instanceof Bathroom){
                System.out.println("Bathroom devices - " + el.getName());
            }
            else {
                System.out.println("Другие приборы - " + el);
            }
        }
    }

    //функция поиска включенного прибора больше заданной мощности
    private static void findDevicesWhichOnAndPower(ArrayList<Electronics> electronics){
        try {
            System.out.println("Введите потребляемую мощность для поиска прибора: ");
            int power = scanner.nextInt();
            for (Electronics e : electronics) {
                if (e.getPower() >= power && e.WorksRightNow()){
                    System.out.println("Включенный прибор больше заданной мощности " + e.getName());
                }
            }
        }
        catch (InputMismatchException e){
            System.out.println("Ошибка! Введено не корректное значение");
        }
    }

    //функция поиска прибора на кухне
    private static void watchWhoUseDevices(Kitchen ... kitchens) {
        String who;
        boolean answerCorrect = false;
        while(!answerCorrect){
            System.out.println("Введите \"mom\" или \"dad\" пользуется устройством: ");
            who = scanner.nextLine();
            if (Objects.equals(who, String.valueOf(Family.dad)) || Objects.equals(who, String.valueOf(Family.mom))){
                answerCorrect = true;
                for (Kitchen k : kitchens) {
                    if (Objects.equals(k.WhoCanUseIt().toLowerCase(), who)){
                        System.out.println(k.getName());}
                }
            }
            else {
                System.out.println("Некорректный ответ (должен быть mom или dad)");
            }
        }
    }

    //ищем общую потребляемую мощность
    private static void findAllPower(ArrayList<Electronics> electronics){
        int allPower = 0;
        for (Electronics electronic : electronics){
            allPower += electronic.getPower();
        }
        System.out.println("Общая потребляемая мощность равна: " + allPower);
    }

    //включаем все устройства в розетку
    private static void setOnAllPowerDevices(ArrayList<Electronics> electronics){
        for (Electronics e : electronics) {
            e.setOn();
        }
    }

    //выключаем все устройства из розетки
    private static void setOffAllPowerDevices(ArrayList<Electronics> electronics){
        for (Electronics e : electronics) {
            e.setOff();
        }
    }

    //функия, которая отображает включение в розетку устройств
    private static void checkPower(ArrayList<Electronics> electronics){
        String condition;
        for (Electronics e : electronics) {
            if (e.WorksRightNow()){
                condition = "on";
            }
            else { condition = "off"; }
            System.out.println("Прибор: " + e.getName() + ", в состоянии: " + condition);
        }
    }

    //функция отключающая питание по максимальному напряжению
    private static void setOffDevicesOverPower(ArrayList<Electronics> electronics){
        try {
            System.out.println("Введите максимальное напряжение устройства для отключения: ");
            int valueOfPower = scanner.nextInt();
            for (Electronics e : electronics) {
                if (e.getPower() >= valueOfPower) {
                    e.setOff();
                }
            }
        }
        catch (InputMismatchException e){
            System.out.println("Ошибка! Введено не корректное значение");
        }
    }

    //функция включающая питание по максимальному напряжению устойства
    private static void setOnDevicesOverPower(ArrayList<Electronics> electronics){
        try{
            System.out.println("Введите максимальное напряжение устройства для включения: ");
            int valueOfPower = scanner.nextInt();
            for (Electronics e: electronics) {
                if (e.getPower() >= valueOfPower){
                    e.setOn();
                }
            }
        }
        catch (InputMismatchException e){
            System.out.println("Ошибка! Введено не корректное значение");
        }
    }

    private static void sortDevicePrices(ArrayList<Electronics> electronics) {
        boolean sorted = false;
        int temp;
        while (!sorted) {
            sorted = true;
            for (int i=0; i < electronics.size()-1; i++) {
                if (electronics.get(i).getPrice() > electronics.get(i+1).getPrice()) {
                    temp = electronics.get(i).getPrice();
                    electronics.get(i).setPrice(electronics.get(i+1).getPrice());
                    electronics.get(i+1).setPrice(temp);
                    sorted = false;
                }
            }
        }
        watchAllTechnics(electronics);
    }

    private static void watchAllTechnics(ArrayList<Electronics> electronics){
        String condition;
        for(Electronics e : electronics){
            if (e.WorksRightNow()){
                condition = "on";
            }
            else {condition = "off";}
            System.out.println("" +
                    "Марка:      " + e.getName() + "\n" +
                    "Состояние:  " + condition + "\n" +
                    "Мощность:   " + e.getPower() + "\n" +
                    "Цена:       " + e.getPrice() + "\n");
        }
    }

    //метод показывающий пример выхода за пределы массива
    private static void findNameOfDevices(ArrayList<Electronics> electronics){
        try
        {
            System.out.print("Введите максимальное количество приборов 1-" + electronics.size() + " : ");
            int count = scanner.nextInt();
            System.out.println(electronics.get(count-1).getName());
        }
        catch (IndexOutOfBoundsException e){
            System.out.println("Ошибка! Введено значение за пределами доступных устройств");
        }
    }

    //Проверка подключения устройств к сети
    private static void checkStatusOfInternet(ArrayList<Electronics> electronics){
        for (Electronics device: electronics) {
            if(device instanceof Bathroom || device instanceof Bedroom){
                System.out.print(device.getName() + " - ");
                ((NetworkAbility) device).internetAvailableStatus();
            }
        }
    }

    //метод бросающий исключение
    private static void checkCatchException() throws CustomException{
        throw new CustomException("This is custom exception");
    }

    //метод для поиска количества устройств на каждого члена семьи
    private static void calculateDevicesPerPerson(ArrayList<Electronics> electronics){
        boolean count = false;
        int howManyPerson = 0;
        try{
            while(!count){
                System.out.print("Введите количество членов семьи (максимум " + electronics.size() + "): ");
                howManyPerson = scanner.nextInt();
                //howManyPerson >= 0 для возможности поставить 0 и поймать исключение
                if (howManyPerson <= electronics.size() && howManyPerson >= 0){
                    count = true;
                    System.out.println("Количество прбиборов на каждого: " + electronics.size() / howManyPerson);
                }
            }
        }
        catch (ArithmeticException e){
            System.out.println("Ошибка! Деление на ноль");
        }
        finally {
            //это здесь для примера использования блока finally
            System.out.println("\nВсего устройств: " + electronics.size());
            System.out.println("Членов семьи: " + howManyPerson);
        }
    }
}
