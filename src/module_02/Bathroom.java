package module_02;

public class Bathroom extends Electronics implements NetworkAbility {
    public Bathroom(String name, boolean worksRightNow, int power, int price) {
        super(name, worksRightNow, power, price);
    }

    @Override
    public void internetAvailableStatus(){
        System.out.println("Нет доступа к сети");
    }
}
