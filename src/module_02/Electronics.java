package module_02;

public class Electronics {
    private final String name;
    private boolean worksRightNow;
    private final int power;
    private int price;

    public Electronics(String name, boolean worksRightNow, int power, int price){
        this.name = name;
        this.worksRightNow = worksRightNow;
        this.power = power;
        this.price = price;
    }

    public String getName(){
        return name;
    }

    public boolean WorksRightNow(){
        return worksRightNow;
    }

    public int getPower(){
        return power;
    }
    public int getPrice(){
        return price;
    }

    public void setPrice(int price){
        this.price = price;
    }

    public void setOn(){
        this.worksRightNow = true;
    }

    public void setOff(){
        this.worksRightNow = false;
    }
}
