package module_02;

public class Bedroom extends Electronics implements NetworkAbility{
    public Bedroom(String name, boolean worksRightNow, int power, int price) {
        super(name, worksRightNow, power, price);
    }

    @Override
    public void internetAvailableStatus() {
        System.out.println("Есть доступ к сети");
    }
}
