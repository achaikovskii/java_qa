package module_02;

public class Kitchen extends Electronics{
    private final String whoCanUseIt;
    public Kitchen(String name, boolean worksRightNow, int power, int price, String whoCanUseIt) {
        super(name, worksRightNow, power, price);
        this.whoCanUseIt = whoCanUseIt;
    }

    public String WhoCanUseIt(){
        return whoCanUseIt;
    }
}
