package module_04;
import java.util.*;

public class WorkWithCollections {
    public static void main(String[] args) {
        startApplication();
    }

    private static void startApplication(){
        ArrayList<Integer> arrayCollection = new ArrayList<>();
        generateArrayCollection(arrayCollection);
        showArrayCollection(arrayCollection);

        HashSet<Integer> A = new HashSet<>();
        generateSetCollection(A, arrayCollection);
        showSetCollection(A);
        checkUniqueElements(A);
        getMinElement(A);
        deleteNotEvenElements(A);
        findPenultimateOfValueElement(A);
    }

    // I. Генерация 1 000 000 последовательных целых чисел
    private static void generateArrayCollection(ArrayList<Integer> collection){
        for (int i=1; i<=1000000; i++){
            collection.add(i);
        }
    }

    private static void showArrayCollection(ArrayList<Integer> collection){
        for (Integer value: collection) {
            System.out.println(value);
        }
    }

    // II. Добавление в коллекцию А элементов в произвольном порядке
    private static void generateSetCollection(HashSet<Integer> setCollection, ArrayList<Integer> arrayCollection){
        setCollection.addAll(arrayCollection);
    }

    // III. Отображение произвольного порядка элементов
    private static void showSetCollection(HashSet<Integer> collection){
        for (Integer value: collection) {
            System.out.println(value);
        }
    }

    // IV. Проверка на уникальность элементов в коллекции.
    private static void checkUniqueElements(HashSet<Integer> collection){
        List<Integer> frequency = collection.stream().distinct().toList();
        if(frequency.size() == collection.size()){
            System.out.println("Повторяющихся элементов нет");
        }
        else {
            System.out.println("Есть повторяющиеся элементы");
        }
    }

    // V. Найдите минимальный элемент последователности
    private static void getMinElement(HashSet<Integer> collection){
        int min = 1000000;
        for (Integer value: collection) {
            if(value < min){
                min = value;
            }
        }
        System.out.println("Минимальный элемент: " + min);
    }

    // VI. Удаление всех нечентных элементов последовательности
    private static void deleteNotEvenElements(HashSet<Integer> collection){
        HashSet<Integer> tmp = new HashSet<>();
        for (Integer value: collection) {
            if(value%2 == 0){
                tmp.add(value);
            }
        }
        collection.clear();
        collection.addAll(tmp);
    }

    // VII. Поиск предпоследнего по величине элемента
    private static void findPenultimateOfValueElement(HashSet<Integer> collection){
        int penultimate = 0;
        int max = 0;
        for (Integer value: collection) {
            penultimate = max;
            if(max < value){
                max = value;
            }
        }
        System.out.println("Предпоследний элемент: " + penultimate);
    }
}

