package module_04;

public class Main {
    public static void main(String[] args) {
        startApplication();
    }

    private static void startApplication(){
        DynamicArray<String> stringDynamicArray = new DynamicArray<>(3);
        stringDynamicArray.add("Test0");
        stringDynamicArray.add("Test1");
        stringDynamicArray.add("Test2");
        stringDynamicArray.add("Test3");
        stringDynamicArray.add("Test4");
        stringDynamicArray.add("Test5");
        stringDynamicArray.add("Test6");


        stringDynamicArray.isString();

        goNextLine();
        stringDynamicArray.remove(0);
        stringDynamicArray.remove(0);
        stringDynamicArray.remove(0);
        stringDynamicArray.remove(0);
        stringDynamicArray.get(0);
        stringDynamicArray.isString();

        stringDynamicArray.add("testtest");
        goNextLine();
        stringDynamicArray.isString();

        goNextLine();
        stringDynamicArray.remove(1);
        stringDynamicArray.isString();

        goNextLine();
        stringDynamicArray.add("12");
        stringDynamicArray.isString();
    }

    private static void goNextLine(){System.out.println();}
}
