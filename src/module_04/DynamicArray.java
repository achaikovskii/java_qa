package module_04;

public class DynamicArray<E> {
    private int size = 0;
    private Object[] array;


    public DynamicArray(int capacity){
        array = new Object[capacity];
    }

    public void add(E e){
        if(array.length <= size) {
            Object[] tmp = array;
            array = new Object[size*2];
            System.arraycopy(tmp, 0, array, 0, tmp.length);
        }
        array[size] = e;
        size++;
    }

    public void get(int index){
        System.out.println(array[index]);
    }

    public void isString(){
        for (int i=0; i < array.length; i++){
            if(array[i] != null){
                System.out.println("Элемент " + i + ": " + array[i]);
            }
        }
    }

    public void remove(int index){
        Object[] tmp = array;
        int count = 0;
        if(array[index] != null){
            array[index] = null;

            for (Object element : array) {
                if (
                        element!= null) {
                    tmp[count] = element;
                    count++;
                }
            }

            for(int i=0; i<tmp.length-1; i++){
                if(tmp[i+1] == null && tmp[i] != null){
                    tmp[i] = null;
                    size = i;
                }
            }
            array = tmp;
        } else { System.out.println("Такого элемента нет"); }
    }
}

